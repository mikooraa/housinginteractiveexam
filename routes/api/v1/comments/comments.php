<?php

Route::prefix('/comment')->name('comment.')->group(function () {
    // Comments LIST
    Route::get('/', 'CommentController@index')->name('index');
    // SHOW Comments
    Route::get('/{id}', 'CommentController@show')->name('show');
    // CREATE Comments
    Route::post('/', 'CommentController@store')->name('store');
    // UPDATE Comments
    Route::put('/{id}', 'CommentController@update')->name('update');
    // DELETE Comments
    Route::delete('/{id}', 'CommentController@delete')->name('archive');
    // RESTORE Comments
    Route::get('/restore/{id}', 'CommentController@restore')->name('restore');

});

