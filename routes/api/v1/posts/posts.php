<?php

Route::prefix('/post')->name('post.')->group(function () {
    // POST LIST
    Route::get('/', 'PostController@index')->name('index');
    // SHOW POST
    Route::get('/{id}', 'PostController@show')->name('show');
    // CREATE POST
    Route::post('/', 'PostController@store')->name('store');
    // UPDATE POST
    Route::put('/{id}', 'PostController@update')->name('update');
    // DELETE POST
    Route::delete('/{id}', 'PostController@delete')->name('archive');
    // RESTORE POST
    Route::get('/restore/{id}', 'PostController@restore')->name('restore');

});

