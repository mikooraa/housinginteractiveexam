## Housing Interactive Exam

Creating a Blog Post using laravel API.

-   Create a blog title and content
-   Show All Blog Post
-   View a Single Post
-   Can Comment on a Post
-   Can Reply on a comment in a Post

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ git clone https://gitlab.com/mikooraa/housinginteractiveexam.git
$ cd [folder_name]              // go to folder of cloned project
$ composer install              // install composer dependencies
$ php artisan migrate --seed    // adding database table and fake data
$ cp .env.example .env          // setup environment file
```
