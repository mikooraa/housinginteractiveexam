<?php

return [
    'fetch_success' => 'Comment fetch success',
    'fetch_failed' => 'Comment fetch failed',
    'update_success' => 'Comment update success',
    'update_failed' => 'Comment update failed',
    'create_success' => 'Comment create success',
    'create_failed' => 'Comment create failed',
    'delete_success' => 'Comment delete success',
    'delete_failed' => 'Comment delete failed',
    'restore_success' => 'Comment restore success',
    'restore_failed' => 'Comment restore failed',
];