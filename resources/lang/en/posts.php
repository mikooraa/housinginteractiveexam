<?php

return [
    'fetch_success' => 'Post fetch success',
    'fetch_failed' => 'Post fetch failed',
    'update_success' => 'Post update success',
    'update_failed' => 'Post update failed',
    'create_success' => 'Post create success',
    'create_failed' => 'Post create failed',
    'delete_success' => 'Post delete success',
    'delete_failed' => 'Post delete failed',
    'restore_success' => 'Post restore success',
    'restore_failed' => 'Post restore failed',
];