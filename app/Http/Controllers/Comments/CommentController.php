<?php

namespace App\Http\Controllers\Comments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Comment\CommentRequest;
use App\Models\Comment;
use Illuminate\Support\Carbon;
use App\Helpers\CommentFilter;
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CommentFilter $filters) {
        if($filters->request->query('status') === "ALL"){
            $comments = Comment::withTrashed()->get();
            return resolveResponse(__('comments.fetch_success'), $comments);
        }else{
            $comments = Comment::withTrashed()->filter($filters)->paginate($filters->limit);
            return resolveResponse(__('comments.fetch_success'), $comments);
        }
        
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $comment = Comment::findOrFail($id);
            return resolveResponse(__('comments.fetch_success'), $comment);
        }catch(\Exception $e) {
            return rejectResponse(__('comments.fetch_failed'), null);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request) {
        \DB::beginTransaction();
        try {
            $comment = Comment::create([
                'post_id' => $request->post_id,
                'parent_id' => $request->parent_id,
                'body' => $request->body,
            ]);
            \DB::commit();
            return resolveResponse(__('comments.create_success'), $comment);
        }catch(\Exception $e) {
            \DB::rollback();
           
            return rejectResponse(__('comments.create_failed'), null);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, $id) {
        \DB::beginTransaction();
        try {
           $comment = Comment::findOrFail($id);

           $comment->update([
                'post_id' => $request->post_id,
                'parent_id' => $request->parent_id,
                'body' => $request->body,
           ]);
            \DB::commit();
            return resolveResponse(__('comments.update_success'), $comment);
        }catch(\Exception $e) {
            \DB::rollback();
           
            return rejectResponse(__('comments.update_failed'), null);
        }
    }

    /**
     * Delete the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        \DB::beginTransaction();
        try {
           $comment = Comment::findOrFail($id);
           $comment->delete();
            \DB::commit();
            return resolveResponse(__('comments.delete_success'), $comment);
        }catch(\Exception $e) {
            \DB::rollback();
            return rejectResponse(__('comments.delete_failed'), null);
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id) {
        \DB::beginTransaction();
        try {
            $comment = Comment::onlyTrashed()->findOrFail($id);
            $comment->restore();
            \DB::commit();
            return resolveResponse(__('comments.restore_success'), $comment);
        }catch(\Exception $e) {
            \DB::rollback();
            return rejectResponse(__('comments.restore_failed'), $e->getMessage());
        }
    }
}
