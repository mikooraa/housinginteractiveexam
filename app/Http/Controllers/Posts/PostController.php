<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Post\PostRequest;
use App\Models\Post;
use Illuminate\Support\Carbon;
use App\Helpers\PostFilter;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostFilter $filters) {
        if($filters->request->query('status') === "ALL"){
            $posts = Post::withTrashed()->get();
            return resolveResponse(__('posts.fetch_success'), $posts);
        }else{
            $posts = Post::withTrashed()->filter($filters)->paginate($filters->limit);
            return resolveResponse(__('posts.fetch_success'), $posts);
        }
        
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        try {
            $post = Post::with(['comments'])->findOrFail($id);
            return resolveResponse(__('posts.fetch_success'), $post);
        }catch(\Exception $e) {
            return rejectResponse(__('posts.fetch_failed'), null);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request) {
        \DB::beginTransaction();
        try {
            $post = Post::create([
                'title' => $request->title,
                'body' => $request->body,
            ]);
            \DB::commit();
            return resolveResponse(__('posts.create_success'), $post);
        }catch(\Exception $e) {
            \DB::rollback();
           
            return rejectResponse(__('posts.create_failed'), null);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id) {
        \DB::beginTransaction();
        try {
           $post = Post::findOrFail($id);

           $post->update([
                'title' => $request->title,
                'body' => $request->body,
           ]);
            \DB::commit();
            return resolveResponse(__('posts.update_success'), $post);
        }catch(\Exception $e) {
            \DB::rollback();
           
            return rejectResponse(__('posts.update_failed'), null);
        }
    }

    /**
     * Delete the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        \DB::beginTransaction();
        try {
           $post = Post::findOrFail($id);
           $post->delete();
            \DB::commit();
            return resolveResponse(__('posts.delete_success'), $post);
        }catch(\Exception $e) {
            \DB::rollback();
            return rejectResponse(__('posts.delete_failed'), null);
        }
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id) {
        \DB::beginTransaction();
        try {
            $post = Post::onlyTrashed()->findOrFail($id);
            $post->restore();
            \DB::commit();
            return resolveResponse(__('posts.restore_success'), $post);
        }catch(\Exception $e) {
            \DB::rollback();
            return rejectResponse(__('posts.restore_failed'), $e->getMessage());
        }
    }
}
