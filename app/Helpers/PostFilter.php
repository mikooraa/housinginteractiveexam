<?php

namespace App\Helpers;

use App\Helpers\QueryFilter;

class PostFilter extends QueryFilter{

    public function search($search=''){
        if(trim($search) && $search != ''){
            $this->filter($search);
        }
    }

    /* Type Column Filtering */
    public function filter($search){
		return $this->builder->where('title', 'LIKE', '%' . $search . '%')
                             ->orWhere('body', 'LIKE', '%' . $search . '%');
    }
} 