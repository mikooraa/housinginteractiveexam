<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

     //API
     protected $comment_api_namespace = 'App\Http\Controllers\Comments';
     protected $post_api_namespace = 'App\Http\Controllers\Posts';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapApiCommentRoutes();

        $this->mapApiPostRoutes();

        //
    }   

     // ITEM ROUTES
     protected function mapApiCommentRoutes()
     {
         Route::prefix('api/v1')
             ->namespace($this->comment_api_namespace)
             ->group(base_path('routes/api/v1/comments/comments.php'));
     }

      // ITEM ROUTES
      protected function mapApiPostRoutes()
      {
          Route::prefix('api/v1')
              ->namespace($this->post_api_namespace)
              ->group(base_path('routes/api/v1/posts/posts.php'));
      }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
