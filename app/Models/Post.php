<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasFilter;
class Post extends Model
{
    use SoftDeletes,HasFilter;

    protected $fillable = ['title', 'body'];

    public function comments()
    {
        return $this->hasMany('App\Models\Comment','post_id')->whereNull('parent_id');
    }
}
