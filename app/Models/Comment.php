<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\HasFilter;
class Comment extends Model
{
    use SoftDeletes, HasFilter;

    protected $with =['replies'];
    protected $fillable = ['post_id', 'parent_id', 'body'];


    public function replies()
    {
        return $this->hasMany('App\Models\Comment', 'parent_id');
    }
}
