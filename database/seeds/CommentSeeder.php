<?php

use Illuminate\Database\Seeder;
use App\Models\Comment;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          factory('App\Models\Comment',10)->create();

          $comments = [
            ['post_id' => 1, 'parent_id' => 0, 'body' => 'sample_body'],
            ['post_id' => 2, 'parent_id' => 0, 'body' => 'sample_body'],
            ['post_id' => 3, 'parent_id' => 0, 'body' => 'sample_body'],
            ['post_id' => 4, 'parent_id' => 0, 'body' => 'sample_body'],
            ['post_id' => 5, 'parent_id' => 0, 'body' => 'sample_body']
          ];

          foreach($comments as $comment){
              Comment::create($comment);
          }
    }
}
